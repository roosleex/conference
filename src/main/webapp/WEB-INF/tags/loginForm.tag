<%@ tag language="java" pageEncoding="UTF-8"%>

<!-- Form --> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			<span class="glyphicon glyphicon-log-in"></span> 
			Log In
		</h3>
	</div> 
	<div class="panel-body">
		<form id="login-form" action="app" method="post">
			<input type="hidden" name="cmd" value="loginCommand">
			<div class="form-group">
				<input type="text" class="form-control" id="uid" name="uid" placeholder="Email">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password">
			</div>
		    <div>
				<button type="submit" class="btn btn-default">Log In</button>
					<a href="reg" style="margin-left:20px;">Registration</a>
			</div>
		</form>
	</div>
</div>
