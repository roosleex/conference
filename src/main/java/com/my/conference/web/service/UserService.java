package com.my.conference.web.service;

import java.util.Optional;

import com.my.conference.dao.DbException;
import com.my.conference.entity.User;

/**
 * Service for a user.
 * @author Ruslan
 *
 */
public interface UserService {
	/**
	 * Registrate a user to the database.
	 * @param user
	 * @return Optional<User> of new user.
	 * @throws DbException
	 */
	Optional<User> registrate(User user) throws DbException;
	
	/**
	 * Login action.
	 * @param uid user's identifier (email).
	 * @param password user's password.
	 * @return Optional<User> of found user.
	 * @throws DbException
	 */
	Optional<User> login(String uid, String password) throws DbException;
}
