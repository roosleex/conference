package com.my.conference.web.filter;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.DispatcherType;

/**
 * This filter sets the application encoding.
 * @author Ruslan
 *
 */
@WebFilter(urlPatterns = "/*", 
initParams = {@WebInitParam(name = "encoding", value = "UTF-8")},
dispatcherTypes = {DispatcherType.REQUEST}
)
public class EncodingFilter implements Filter {
	private static final Logger LOG = LogManager.getLogger(EncodingFilter.class);
	private String encoding;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		encoding = filterConfig.getInitParameter("encoding");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String characterEncoding = req.getCharacterEncoding();
		LOG.debug("Current characterEncoding: " + characterEncoding);
		if (characterEncoding == null) {
			LOG.debug("set encoding: ", encoding);
			req.setCharacterEncoding(encoding);
		}
		chain.doFilter(request, response);
	}
}
