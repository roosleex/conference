package com.my.conference.web.command;

import java.sql.Timestamp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.DbException;
import com.my.conference.entity.User;
import com.my.conference.web.Util;
import com.my.conference.web.service.UserService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The command for a user's registration.
 * @author Ruslan
 *
 */
public class UserRegCommand extends Command {
	private static final Logger LOG = LogManager.getLogger(UserRegCommand.class);
	private final UserService userService;
	
	public UserRegCommand(UserService userService) {
		super("user_reg");
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		User user = null;
		try {
			String uid = req.getParameter("email").trim();
			String pwd = req.getParameter("pwd");
			String pwd2 = req.getParameter("pwd2");
			
			String birthdate = req.getParameter("birthdate");
			Timestamp birthdateTS = null;
			if (birthdate != "") {
				birthdateTS = Timestamp.valueOf(Util.getFullDateForTimestamp(birthdate));
			}
			
			if (!pwd.equals(pwd2)) {
				throw new CommandException("Repeated password is not equals password!", req);
			}
			
			//LOG.debug("req.getParameter(\"birthdate\") : " + req.getParameter("birthdate"));
			
			user = new User(uid, 
					pwd.trim(), 
					birthdateTS, 
					req.getParameter("firstname"), 
					req.getParameter("lastname"));
			userService.registrate(user);
		} catch (DbException | IllegalArgumentException e) {
			String msg = "Cannot registrate user: ";
			LOG.error(msg + user + ". " + e.getMessage(), user);
			throw new CommandException(msg + ". " + e.getMessage(), e, req);
		}
		return "login";
//		return "redirect:app?cmd=allProducts";
	}
	
}
