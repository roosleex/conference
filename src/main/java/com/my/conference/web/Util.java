package com.my.conference.web;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Utility methods.
 * @author Ruslan
 *
 */
public class Util {
	/**
	 * Generate hash for a password.
	 * @param input not hashed string.
	 * @return hash 
	 */
	public static String generateHash(String input) {
	    byte[] in = input.getBytes(StandardCharsets.UTF_8);
		MessageDigest md;
	    try {
	        md = MessageDigest.getInstance("MD5");
	    } catch (NoSuchAlgorithmException e) {
	        throw new IllegalArgumentException(e);
	    }
	    byte[] bytes = md.digest(in);
	    
	    StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
	}
	
	/**
	 * Get full date for a timestamp, for example if dt = 2022-01-01 then 
	 * method will return 2022-01-01 00:00:00  
	 * @param dt string like YYYY-MM-DD
	 * @return string like YYYY-MM-DD HH:MM:SS or original dt value.
	 */
	public static String getFullDateForTimestamp(String dt) {
		if (dt.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
			return dt + " 00:00:00";
		}
		
		return dt;
	}
	
	/**
	 * Set request attribute with a value of error message.
	 * @param req HttpServletRequest
	 * @param msg error message
	 */
	public static void setRequestErrorMsgAttr(HttpServletRequest req, String msg) {
		req.setAttribute("errorMessage", Util.appendHTMLBackLink(msg, req.getHeader("referer")));
	}
	
	/**
	 * Append HTML back link to the end of text.
	 * @param text.
	 * @param href href value of a link.
	 * @return appended text.
	 */
	public static String appendHTMLBackLink(String text, String href) {
		return text + "<br><br><a href=\"" + href + "\">Return back</a>";
	}
	
	/**
	 * Get user's full years age.
	 * @param birthDate user's date of birth.
	 * @return full years value.
	 */
	public static int getUserAgeFullYears(Timestamp birthDate) {
		int age = 0;
		Calendar birthDt = Calendar.getInstance();
		birthDt.setTime(new Date(birthDate.getTime()));
		Calendar currDt = Calendar.getInstance();
		currDt.setTime(new Date());
	    if (birthDt.get(Calendar.YEAR) >= currDt.get(Calendar.YEAR)) {
	        return age;
	    }
	        
	    //calculate month difference from current date in time  
	    long monthDiff = currDt.getTimeInMillis() - birthDate.getTime();  
	        
	    //convert the calculated difference in date format
	    Calendar ageDt = Calendar.getInstance();
	    ageDt.setTime(new Date(monthDiff));
	        
	    //extract year from date      
	    int year = ageDt.get(Calendar.YEAR);  
	        
	    //now calculate the age of the user  
	    age = Math.abs(year - 1970);
	    //alert('age=' + age);
	    return age;
	}
}
