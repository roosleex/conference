package com.my.conference.entity;

import java.io.Serializable;

/**
 * The persistent class for the role database table.
 * 
 */
public class Role extends BaseEntity implements Serializable {
	/**
	 *  Predefined moderator id.
	 */
	public static final int MODERATOR_ID = 1;
	/**
	 *  Predefined speaker id.
	 */
	public static final int SPEAKER_ID = 2;
	/**
	 *  Predefined simple user id.
	 */
	public static final int USER_ID = 3;
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;

	public Role(String name) {
		// id is autoincrement.
		this.name = name;
	}
	
	public Role(int id, String name) {
		this(name);
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}