package com.my.conference.entity;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Base entity class.
 * @author Ruslan
 *
 */
public abstract class BaseEntity {
	/**
	 * Validate class fields values.
	 * @param validData data for validation. Output parameter. Here we
	 * receive a map with description of some entity class fields 
	 * for validation.
	 * @return true when all fields are valid otherwise false.
	 */
	public boolean validate(Map<String, Validator> validData) {
		return Validator.validateAll(validData);
	}
}
