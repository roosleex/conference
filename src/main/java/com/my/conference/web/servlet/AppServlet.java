package com.my.conference.web.servlet;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.web.command.Command;
import com.my.conference.web.command.CommandContainer;
import com.my.conference.web.command.CommandException;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This is the main controller.
 * @author Ruslan
 *
 */
@WebServlet("/app")
public class AppServlet extends HttpServlet {
	private static final Logger LOG = LogManager.getLogger(AppServlet.class);
	//private CommandContainer commands;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		//commands = (CommandContainer) config.getServletContext().getAttribute("commandContainer");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOG.debug(getClass().getName() + "#doGet");
		try {
			String url = getUrl(req, resp);
			LOG.debug("url: {}", url);
			resp.sendRedirect(url);
			LOG.debug("redirected: " + url, url);
			//req.getRequestDispatcher(url).forward(req, resp);
			//LOG.debug("forward: " + url, url);
		} catch (CommandException e) {
			LOG.error("Error: {}", e);
			resp.sendError(500, "Cannot process the command");
//			throw new ServletException("Cannot process the command", e);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOG.debug(getClass().getName() + "#doPost");
		try {
			String url = getUrl(req, resp);
			resp.sendRedirect(url);
			LOG.debug("redirected: " + url, url);
		} catch (CommandException e) {
			LOG.error("Error: " + e.getMessage(), e);
			LOG.trace("HttpServletRequest getAttribute 'errorMessage': " + req.getAttribute("errorMessage"));
			resp.sendError(500, "Cannot process the command");
//			throw new ServletException("Cannot process the command", e);
		}
	}
	
	private String getUrl(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String cmdName = req.getParameter("cmd");
		LOG.debug("cmdName: " + cmdName);
		CommandContainer commands = (CommandContainer) req.getServletContext().getAttribute("commandContainer");
		Command command = commands.get(cmdName);
		LOG.debug("command: " + command.getClass().getSimpleName(), command);
		String url = command.execute(req, resp);
		return url;
	}
}
