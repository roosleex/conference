package com.my.conference.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Base class for a command.
 * @author Ruslan
 *
 */
public abstract class Command {
	/**
	 * Name of action from sql table Action.
	 */
    public final String ACTION_NAME;
    
	protected Command(String actionName) {
		ACTION_NAME = actionName;
	}
	
	/**
	 * Here we execute a command.
	 * @param req
	 * @param resp
	 * @return String url for redirecting to.
	 * @throws CommandException
	 */
	public abstract String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;

	@Override
	public String toString() {
		return "Command=[" + getClass().getSimpleName() + "]";
	}
}
