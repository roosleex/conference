package com.my.conference.entity;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Validate some entity field value.
 * @author Ruslan
 *
 */
public class Validator {
	/**
	 * Value for validation.
	 */
	private String value;
	/**
	 * Regular expression.
	 */
	private final String regexp;
	/**
	 * Error message.
	 */
	private final String errorMessage;
	/**
	 * Error detailed message.
	 */
	private final String errorDetails;
	/**
	 * Status of validation.
	 */
	private boolean isValid;
	
	public Validator(String value, String regexp, String errorMessage, String errorDetails) {
		this.value = value;
		this.regexp = regexp;
		this.errorMessage = errorMessage;
		this.errorDetails = errorDetails;
		this.isValid = false;
	}
	
	public String getValue() {
		return value;
	}
	public String getRegexp() {
		return regexp;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public String getErrorDetails() {
		return errorDetails;
	}
	public boolean getIsValid() {
		return isValid;
	}
	
	/**
	 * Validate some field value of entity class.
	 * @return true when a field is valid otherwise false.
	 */
	public boolean validate() {
		boolean result = value.matches(regexp);
		isValid = result;
		return result;
	}
	
	/**
	 * Concatenate all errors text to one String. 
	 * @param validData validation data.
	 * @return String with errors text.
	 */
	public static String getErrorsText(Map<String, Validator> validData) {
		String result = "<ul>";
		boolean wasErr = false;
		for (Entry<String, Validator> e : validData.entrySet()) {
			Validator v = e.getValue();
			if (!v.getIsValid()) {
				wasErr = true;
				if (v.getErrorMessage() != "") {
				    result = result + "<li>" + v.getErrorMessage();
				    
				    if (v.getErrorDetails() != "") {
						result = result + v.getErrorDetails(); 
					}
				    
				    result = result + "</li>"; 
				}
			}
		}
		
		if (wasErr) {
			result = result + "</ul>";
		} else {
			result = "";
		}
		
		return result;		
	}
	
	/**
	 * Validate all entries in validData.
	 * @param validData validation data.
	 * @return true when all entries are valid otherwise false.
	 */
	public static boolean validateAll(Map<String, Validator> validData) {
		 boolean result = true;
			for (Entry<String, Validator> e : validData.entrySet()) {
				boolean res = e.getValue().validate();
				if (!res) {
					result = res;
				}
			}
		return result;
	}
}
