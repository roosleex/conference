package com.my.conference.web.command;

import java.sql.Timestamp;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.DbException;
import com.my.conference.entity.User;
import com.my.conference.web.Util;
import com.my.conference.web.service.UserService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The command for a user's login.
 * @author Ruslan
 *
 */
public class LogoutCommand extends Command {
	private static final Logger LOG = LogManager.getLogger(LogoutCommand.class);
	private final UserService userService;
	
	public LogoutCommand(UserService userService) {
		super("user_login");
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		HttpSession sess = req.getSession();
		sess.invalidate();
        LOG.debug("LogoutCommand#execute");
		return "home";
//		return "redirect:app?cmd=allProducts";
	}
	
}
