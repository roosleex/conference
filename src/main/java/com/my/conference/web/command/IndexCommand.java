package com.my.conference.web.command;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The default command for the start page.
 * @author Ruslan
 *
 */
public class IndexCommand extends Command {
	public IndexCommand() {
		//There is no sql action for the index command.
		super("");
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		return "home";
	}
}
