package com.my.conference.web.command;

import java.util.HashMap;
import java.util.Map;

/**
 * The container for commands.
 * @author Ruslan
 *
 */
public class CommandContainer {
	private Map<String, Command> commands = new HashMap<>();
	
	/**
	 * Add a command to the container.
	 * @param name command name.
	 * @param command command object.
	 */
	public void add(String name, Command command) {
		commands.put(name, command);
	}
	
	/**
	 * Get a command from the cotainer.
	 * @param name command name.
	 * @return Command
	 */
	public Command get(String name) {
		return commands.get(name);
	}

	@Override
	public String toString() {
		return "CommandContainer [commands=" + commands + "]";
	}
	
	
}
