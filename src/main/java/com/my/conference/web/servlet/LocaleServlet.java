package com.my.conference.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This controller sets language of the application.
 * @author Ruslan
 *
 */
@WebServlet("/lang")
public class LocaleServlet extends HttpServlet {
	//private static final long serialVersionUID = 1L;
	private static final Logger LOG = LogManager.getLogger(LocaleServlet.class);
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String locale = req.getParameter("lang");
		
		// TODO good way to put locale name into session
		
		resp.addCookie(new Cookie("defaultLocale", locale));
		
		LOG.debug("Set locale: " + locale);
		
		String referer = req.getHeader("referer");
		resp.sendRedirect(referer);
	}
}
