package com.my.conference.web.listener;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.UserDao;
import com.my.conference.dao.mysql.UserDaoImpl;
import com.my.conference.web.command.Command;
import com.my.conference.web.command.CommandContainer;
import com.my.conference.web.command.IndexCommand;
import com.my.conference.web.command.LoginCommand;
import com.my.conference.web.command.LogoutCommand;
import com.my.conference.web.command.UserRegCommand;
import com.my.conference.web.service.UserService;
import com.my.conference.web.service.UserServiceImpl;
import com.my.conference.web.servlet.AppServlet;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionListener;

/**
 * Context of the application.
 * @author Ruslan
 *
 */
@WebListener
public class ContextListener implements ServletContextListener, HttpSessionListener {
	private static final Logger LOG = LogManager.getLogger(ContextListener.class);

	/**
	 * Bootstrap of the application 
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		setLogFile(context);
		LOG.debug("Start context initialization");
		
		initDatasource(context);
		initServicesAndCommands(context);
		
		LOG.debug("End context initialization");
	}

	/**
	 * Set file for saving log data.
	 * @param context application context.
	 */
	private void setLogFile(ServletContext context) {
		String path = context.getRealPath("/WEB-INF/logs/log4j2.log");
		System.setProperty("logFileName", path);
		
		// reconfigure loggers with paths
	    org.apache.logging.log4j.core.LoggerContext ctx =
	            (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
	    ctx.reconfigure();
	    
	    LOG.debug("======= APPLICATION HAS STARTED! =================================================");
	    LOG.debug("Log file path: " + path);
	}

	/**
	 * Add data source to context.
	 * @param context application context.
	 * @throws IllegalStateException
	 */
	private void initDatasource(ServletContext context) throws IllegalStateException {
		String dataSourceName = context.getInitParameter("dataSource");
		Context jndiContext = null;
		try {
			jndiContext = (Context) new InitialContext().lookup("java:/comp/env");
			DataSource dataSource = (DataSource) jndiContext.lookup(dataSourceName); // "jdbc/mysql"
			context.setAttribute("dataSource", dataSource);
			LOG.trace("context.setAttribute 'dataSource': ", dataSource.getClass().getName());
		} catch (NamingException e) {
			LOG.error("Cannot initialize dataSource. " + e.getMessage());
			throw new IllegalStateException("Cannot initialize dataSource", e);
		}
		LOG.debug("DataSource initialized!");
	}

    /**
     * Add services and commands to context.
     * @param context application context.
     */
	private void initServicesAndCommands(ServletContext context) {
		// Create dao for services.
		UserDao userDao = new UserDaoImpl((DataSource) context.getAttribute("dataSource"));
		LOG.trace("Created 'userDao': ", userDao);

		// create services---
		UserService userService = new UserServiceImpl(userDao);
		context.setAttribute("userService", userService);
		LOG.trace("context.setAttribute 'userService': ", userService);
		LOG.debug("Services initialized!");
		//-------------------
		
		//create commands---
		CommandContainer commands = new CommandContainer();
		Command command = new IndexCommand();
		commands.add(null, command);
		commands.add("", command);
		LOG.trace("commands.add IndexCommand");
		
		command = new UserRegCommand(userService);
		commands.add("userRegCommand", command);
		LOG.trace("commands.add UserRegCommand");
		
		command = new LoginCommand(userService);
		commands.add("loginCommand", command);
		LOG.trace("commands.add LoginCommand");
		
		command = new LogoutCommand(userService);
		commands.add("logoutCommand", command);
		LOG.trace("commands.add LogoutCommand");

		context.setAttribute("commandContainer", commands);
		LOG.trace("context.setAttribute 'commandContainer': ", commands);
		LOG.debug("Commands initialized!");
		//-----------------------------
	}

}
