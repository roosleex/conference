/**
 * Code for validating login form.
 */
function validateLoginForm() {
	var PWD_MIN_LEN = 8;
  	var PWD_MAX_LEN = 16;
  	
	$.validator.addMethod("pwdPattern",
  		function(value, element, regexp) {
  	        //alert(regexp);
  	        	var re = new RegExp(regexp);
  	     		//alert(re);
  	    		return this.optional(element) || re.test(value);
  	    	}, 
  	    	"Password incorrect format."
  	    );
  	     	
  	$("#login-form").validate({
  	    rules: {
  	    	uid: {
  				required: true,
  				minlength: 6,
  				email: true
  			},
  			pwd: {
  				required: true,
  			    minlength: PWD_MIN_LEN,
  			    maxlength: PWD_MAX_LEN,
  			    pwdPattern: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -\/:-@\[-`{-~]).{" + PWD_MIN_LEN + "," + PWD_MAX_LEN + "}$"
  			},
  	    },
  	    messages: {
  	    	uid: {
  				required: "Email is required!",
  				minlength: "Email must be at least 6 characters!",
  				email: "Email wrong format!"
  			},
  			pwd: {
  				required: "Password is required!",
  			    minlength: "Password must be at least " + PWD_MIN_LEN + " characters!",
  			    maxlength: "Password must be no more than " + PWD_MAX_LEN + " characters!"
  			},
  	    }
  	});
}