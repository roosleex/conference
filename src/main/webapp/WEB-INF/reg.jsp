﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!-- Template by Quackit.com -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Registration</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS: You can use this stylesheet to override any Bootstrap styles and/or apply your own styles -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		.valid {
			border: 1px solid green;
		}

		.error {
			color: red;
		}
	</style>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-globe"></span> Logo</a>
            </div>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="home">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Products</a>
                    </li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
						<ul class="dropdown-menu" aria-labelledby="about-us">
							<li><a href="#">Engage</a></li>
							<li><a href="#">Pontificate</a></li>
							<li><a href="#">Synergize</a></li>
						</ul>
					</li>    
                </ul>

				<!-- Search -->
				<form class="navbar-form navbar-right" role="search">
					<div class="form-group">
						<input type="text" class="form-control">
					</div>
					<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
				</form>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

	<div class="container-fluid">

		<!-- Left Column -->
		<div class="col-sm-3">	
				
		</div><!--/Left Column-->
  
		<!-- Center Column -->
		<div class="col-sm-6">
		
			<!-- Alert -->
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&#x02E3;</span></button>
				Fill in the fields below
			</div>		
		
			<!-- Form --> 
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						<span class="glyphicon glyphicon-plus"></span> 
						Registration
					</h3>
				</div>
				<div class="panel-body">
					<form id="reg-form" action="app" method="post">
					    <input type="hidden" name="cmd" value="userRegCommand"><br>
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" placeholder="Email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="pwd2" name="pwd2" placeholder="Repeat password">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name">
						</div>
						<div class="form-group">
							<label>Date of birth: 
								<input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Birth date">
							</label>
						</div>
						<div>
							<button type="submit" class="btn btn-default">OK</button>
						</div>
					</form>
				</div>
			</div>
		</div><!--/Center Column-->


	  <!-- Right Column -->
	  <div class="col-sm-3">

	  </div><!--/Right Column -->

	</div><!--/container-fluid-->
	
	<footer>
		<div class="footer-blurb">
			<div class="container">
				<div class="row">

				</div>
				<!-- /.row -->	
			</div>
        </div>
        
        <div class="small-print">
        	<div class="container">
        		<p><a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy Policy</a> | <a href="#">Contact</a></p>
        		<p>Copyright &copy; Example.com 2015 </p>
        	</div>
        </div>
	</footer>

	
    <!-- jQuery -->
    <!-- <script src="js/jquery-1.11.3.min.js"></script> -->
    <script
  		src="https://code.jquery.com/jquery-3.6.0.min.js"
  		integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  		crossorigin="anonymous"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!-- IE10 viewport bug workaround -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
	
	<!-- Placeholder Images -->
	<script src="js/holder.min.js"></script>
	
	<!-- Form validator -->
  	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
  	
  	<script type="text/javascript">
  	    var MIN_AGE = 16;   
  	    var MAX_AGE = 100;
  	    var PWD_MIN_LEN = 8;
  	    var PWD_MAX_LEN = 16;
  	    
  	    $(document).ready(function() {
  	        $.validator.addMethod("birthdateMinVal", 
  	        	function (value, element) {
  	        		//alert('value=' + value + ' element=' + element);
  	        		var check = false;
  	    			var age = getUserAgeFullYears(value);
  	    			if ((age >= MIN_AGE) && (age <= MAX_AGE)) {
  	    				check = true;
  	    			}
  	    			return check;
  	        	}, 
  	        	"User's birthdate must be from " + MIN_AGE +" to " + MAX_AGE + "!"
  	        );
  	        
  	     	$.validator.addMethod("pwdPattern",
  	    		function(value, element, regexp) {
  	    		    //alert(regexp);
  	     		    var re = new RegExp(regexp);
  	     		    //alert(re);
  	    		    return this.optional(element) || re.test(value);
  	    		}, 
  	    		"Password incorrect format."
  	        );
  	    	
  	     	$.validator.addMethod("firstnamePattern",
  	  	    	function(value, element, regexp) {
  	     			//alert(regexp);	
  	     			var re = new RegExp(regexp);
  	  	    		return this.optional(element) || re.test(value);
  	  	    	}, 
  	  	    	"First name incorrect format."
  	  	    );
  	        
  	     	$.validator.addMethod("lastnamePattern",
  	  	    	function(value, element, regexp) {
  	  	     		var re = new RegExp(regexp);
  	  	    		return this.optional(element) || re.test(value);
  	  	        }, 
  	  	    	"Last name incorrect format."
  	  	    );
  	     	
  	    	$("#reg-form").validate({
  	    		rules: {
  	    			email: {
  						required: true,
  						minlength: 6,
  						email: true
  					},
  			        pwd: {
  			        	required: true,
  			        	minlength: 8,
  			        	maxlength: 16,
  			        	pwdPattern: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -\/:-@\[-`{-~]).{" + PWD_MIN_LEN + "," + PWD_MAX_LEN + "}$"
  			        },
  					pwd2: {
  			        	required: true,
  			        	equalTo: "#pwd"
  			        },
  			        firstname: {
			        	required: true,
			        	firstnamePattern: "^[A-Za-zА-Яа-я]{1}[A-Za-zА-Яа-я-]{1,44}$"
  			        },
			        lastname: {
  			        	required: true,
  			        	lastnamePattern: "^[A-Za-zА-Яа-я]{1}[A-Za-zА-Яа-я-]{1,44}$"
  			        },
  			        birthdate: {
  			        	required: true,
  			        	dateISO: true,
  			        	birthdateMinVal: true
  			        }
  	    		},
  	    		messages: {
  	    			email: {
  						required: "Email is required!",
  						minlength: "Email must be at least 6 characters!",
  						email: "Email wrong format!"
  					},
  			        pwd: {
  			        	required: "Password is required!",
  			        	minlength: "Password must be at least " + PWD_MIN_LEN + " characters!",
  			        	maxlength: "Password must be no more than " + PWD_MAX_LEN + " characters!"
  			        },
  					pwd2: {
  			        	required: "Repeated password is required!",
  			        	equalTo: "Repeated password must be equal to password!"
  			        },
  			        firstname: {
			        	required: "First name is required!",
			        	//minlength: 8
			        },
			        lastname: {
  			        	required: "Last name is required!",
  			        	//minlength: 8
  			        },
  			        birthdate: {
  			        	required: "Date of birth is required!",
  			        	dateISO: "Date of birth is not valid!"
  			        }
  	    		}
  	    	});
  		});
  	    
  	    //Get value of user age in full years.
  	    //dob - date of birth in string YYYY-MM-DD
  	    function getUserAgeFullYears(dob) {
  	    	var age = 0;
  	    	var birthDt = new Date(dob);
  	        var currDt = new Date();
  	    	//alert(currDt.getFullYear());
  	        if (birthDt.getFullYear() >= currDt.getFullYear()) {
  	        	return age;
  	        }
  	        
  	        //calculate month difference from current date in time  
  	        var month_diff = Date.now() - birthDt.getTime();  
  	        
  	        //convert the calculated difference in date format  
  	        var age_dt = new Date(month_diff);   
  	        
  	        //extract year from date      
  	        var year = age_dt.getUTCFullYear();  
  	        
  	        //now calculate the age of the user  
  	        age = Math.abs(year - 1970);
  	        //alert('age=' + age);
  	        return age;
  	    }
  	</script>
	
</body>

</html>
