package com.my.conference.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import com.my.conference.web.Util;


/**
 * The persistent class for the user database table.
 * 
 */
public class User extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Minimal length of password.
	 */
	public static final int PWD_MIN_LEN = 8;
	/**
	 * Maximal length of password.
	 */
	public static final int PWD_MAX_LEN = 16;
	/**
	 * Minimal user's age.
	 */
	public static final int MIN_AGE = 16;
	/**
	 * Maximal user's age.
	 */
	public static final int MAX_AGE = 100;
	
	private int id;
	private String email;
	private String password;
	private Timestamp birthDate;
	private String firstName;  
	private String lastName;
	private Timestamp createTime;
	private int roleId;

	
    /**
     * Instantiate user with simple user default role id (simple user)
     * and autofilling user time creation.
     * @param email user login (email at the same time).
     * @param password password.
     * @param birthDate user birth date.
     * @param firstName user first name.
     * @param lastName user last name.
     */
	public User(String email, String password, Timestamp birthDate,  String firstName, String lastName) {
		// id is autoincrement.
		this.email = email;
		this.password = password;
		if (!(birthDate instanceof Timestamp)) {
			throw new IllegalArgumentException("User's birthdate wrong format!");
		}
		if (!isBirthDateInRange(birthDate)) {
			throw new IllegalArgumentException("User's birthdate must be from " + MIN_AGE +" to " + MAX_AGE + "!");
		}
		this.birthDate = birthDate;
		this.firstName = firstName;
		this.lastName = lastName;
		// will be filled by sql
		//this.createTime = createTime;
		this.roleId = Role.USER_ID;
	}
	
	/**
     * Instantiate user with ability to assign all fields.
     * @param id user id.
     * @param email user login (email at the same time).
     * @param password password.
     * @param birthDate user birth date.
     * @param firstName user first name.
     * @param lastName user last name.
     * @param createTime time of creation of user.
     * @param role role id.
     */
	public User(int id, String email, String password, Timestamp birthDate,  String firstName, String lastName,
			Timestamp createTime, int role) {
		this(email, password, birthDate, firstName, lastName);
		this.id = id;
		if (!(createTime instanceof Timestamp)) {
			throw new IllegalArgumentException("User's time of creation wrong format!");
		}
		this.createTime = createTime;
		this.roleId = role;
	}

	public Timestamp getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Timestamp birthDate) {
		this.birthDate = birthDate;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public String getEmail() {
		return this.email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getId() {
		return this.id;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(birthDate, createTime, email, firstName, id, lastName, password, roleId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(birthDate, other.birthDate) && Objects.equals(createTime, other.createTime)
				&& Objects.equals(email, other.email) && Objects.equals(firstName, other.firstName) && id == other.id
				&& Objects.equals(lastName, other.lastName) && Objects.equals(password, other.password)
				&& roleId == other.roleId;
	}

	@Override
	public String toString() {
		return "User [birthDate=" + birthDate + ", createTime=" + createTime + ", email=" + email + ", firstName="
				+ firstName + ", id=" + id + ", lastName=" + lastName + ", password=" + password + ", roleId=" + roleId
				+ "]";
	}

	@Override
	public boolean validate(Map<String, Validator> validData) {
		validData.put("id", new Validator(String.valueOf(id), "^\\d+$", "Incorrect id!", ""));
		validData.put("email", getEmailValidator(email));
		validData.put("password", getPasswordValidator(password));
		validData.put("birthDate", new Validator(birthDate.toString(), "^\\d{4}-\\d{2}-\\d{2}( \\d{2}:\\d{2}:\\d{2}.\\d{1})?$", "Incorrect birth date!", ""));
		validData.put("firstName", new Validator(firstName, "^[[A-Za-zА-Яа-я]+[A-Za-zА-Яа-я-]*]{2,45}$", "Incorrect first name!", ""));
		validData.put("lastName", new Validator(lastName, "^[[A-Za-zА-Яа-я]+[A-Za-zА-Яа-я-]*]{2,45}$", "Incorrect last name!", ""));
		//validData.put("createTime", new Validator(createTime.toString(), "^\\\\d{4}-\\\\d{2}-\\\\d{2}$", "Incorrect time creation!", ""));
		validData.put("roleId", new Validator(String.valueOf(roleId), "\\d+", "Incorrect role id!", ""));

		return super.validate(validData);
	}
	
	/**
	 * Get validator for the email.
	 * @param value email value.
	 * @return Validator.
	 */
	public static Validator getEmailValidator(String value) {
		return new Validator(value, "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", "Incorrect email!", "");
	}
	
	/**
	 * Get validator for the password.
	 * @param value not hashed password value.
	 * @return Validator.
	 */
	public static Validator getPasswordValidator(String value) {
		return new Validator(value, "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{" + PWD_MIN_LEN + "," + PWD_MAX_LEN + "}$", "Incorrect password!", 
				"<br>Password must contain at least one digit [0-9]." +
				"<br>Password must contain at least one lowercase Latin character [a-z]." +
				"<br>Password must contain at least one uppercase Latin character [A-Z]." + 
				"<br>Password must contain at least one special character like ! @ # & ( )." /*+
				"<br>Password must contain a length of at least " + PWD_MIN_LEN + " characters and a maximum of " + PWD_MAX_LEN + " characters."*/
				);
	}
	
	/**
	 * Check if a year of user's birth date is in range between MIN_AGE
	 * and MAX_AGE
	 * @param birthDate
	 * @return
	 */
	private boolean isBirthDateInRange(Timestamp birthDate) {
		int age = Util.getUserAgeFullYears(birthDate);
		return ((age >= MIN_AGE) && (age <= MAX_AGE));
	}

}