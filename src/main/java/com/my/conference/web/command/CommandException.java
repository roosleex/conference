package com.my.conference.web.command;

import com.my.conference.web.Util;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Exception for a command.
 * @author Ruslan
 *
 */
public class CommandException extends Exception {
	public CommandException() {
		super();
	}

	public CommandException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CommandException(String message, Throwable cause) {
		super(message, cause);
	}

	public CommandException(String message) {
		super(message);
	}

	public CommandException(Throwable cause) {
		super(cause);
	}
	
	public CommandException(String message, Throwable cause, HttpServletRequest req) {
		super(message, cause);
		Util.setRequestErrorMsgAttr(req, message);
	}

	public CommandException(String message, HttpServletRequest req) {
		super(message);
		Util.setRequestErrorMsgAttr(req, message);
	}
	
}
