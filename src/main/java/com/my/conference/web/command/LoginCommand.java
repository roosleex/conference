package com.my.conference.web.command;

import java.sql.Timestamp;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.DbException;
import com.my.conference.entity.User;
import com.my.conference.web.Util;
import com.my.conference.web.service.UserService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * The command for a user's login.
 * @author Ruslan
 *
 */
public class LoginCommand extends Command {
	private static final Logger LOG = LogManager.getLogger(LoginCommand.class);
	private final UserService userService;
	
	public LoginCommand(UserService userService) {
		super("user_login");
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
		String uid = req.getParameter("uid").trim();
		String password = req.getParameter("pwd").trim();
		try {
			Optional<User> user = userService.login(uid, password);
			if (user.isPresent()) {
				HttpSession sess = req.getSession();
				sess.setAttribute("user", user.get());
				LOG.debug("LoginCommand#execute session.setAttribute 'user': " + user.get());
			}
		} catch (DbException e) {
			String msg = e.getMessage();
			LOG.error(msg);
			throw new CommandException(msg, e, req);
		}
		return "home";
//		return "redirect:app?cmd=allProducts";
	}
	
}
