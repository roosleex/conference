<%@ tag language="java" pageEncoding="UTF-8"%>

<!-- User menu -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title"><span class="glyphicon glyphicon-random"></span> Completely Synergize</h1>
	</div>
	<div class="list-group">
		<a href="#" class="list-group-item">Resource Taxing</a>
		<a href="#" class="list-group-item">Premier Niche Markets</a>
		<a href="#" class="list-group-item">Dynamically Innovate</a>
		<a href="#" class="list-group-item">Objectively Innovate</a>
		<a href="app?cmd=logoutCommand" class="list-group-item">Logout</a>
	</div>
</div>
