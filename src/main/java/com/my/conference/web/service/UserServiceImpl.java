package com.my.conference.web.service;


import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.DbException;
import com.my.conference.dao.UserDao;
import com.my.conference.entity.User;
import com.my.conference.entity.Validator;

/**
 * Service implementation for a user.
 * @author Ruslan
 *
 */
public class UserServiceImpl implements UserService {
	private static final Logger LOG = LogManager.getLogger(UserServiceImpl.class);
	private final UserDao userDao;
	
	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public Optional<User> registrate(User user) throws DbException {
		LOG.debug("UserServiceImpl.registrate user: " + user);
		Map<String, Validator> validData = new HashMap<>();;
		if (!user.validate(validData)) {
			String errTxt = "User validation failed! : " + Validator.getErrorsText(validData);
			LOG.error(errTxt);
			throw new DbException(errTxt);
		}
		return userDao.registrate(user);
	}
	
	@Override
	public Optional<User> login(String uid, String password) throws DbException {
		LOG.debug("UserServiceImpl.login uid: " + uid);
		Map<String, Validator> validData = new HashMap<>();
		validData.put("email", User.getEmailValidator(uid));
		validData.put("password", User.getPasswordValidator(password));
		if (!Validator.validateAll(validData)) {
			String errTxt = "User login failed! Incorrect email or password!";
			//String errTxt = "User login failed! : " + Validator.getErrorsText(validData);
			LOG.error(errTxt + " : " + Validator.getErrorsText(validData));
			throw new DbException(errTxt);
		}
		return userDao.login(uid, password);
		
	}

}
