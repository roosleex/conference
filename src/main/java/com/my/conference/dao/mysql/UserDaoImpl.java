package com.my.conference.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Optional;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.my.conference.dao.DbException;
import com.my.conference.dao.UserDao;
import com.my.conference.entity.User;
import com.my.conference.web.Util;
import com.my.conference.web.service.UserServiceImpl;

/**
 * User dao implementation.
 * @author Ruslan
 *
 */
public class UserDaoImpl implements UserDao {
	private static final Logger LOG = LogManager.getLogger(UserServiceImpl.class);
    private final DataSource ds;
    private static final String INSERT_NEW_USER = "INSERT INTO user (password,first_name,last_name,role_id,email,birth_date) VALUES (?,?,?,?,?,?)";
	private static final String SELECT_USER_BY_EMAIL = "SELECT id,password,create_time,first_name,last_name,role_id,email,birth_date from user WHERE email=?";
	private static final String SELECT_USER_BY_ID = "SELECT id,password,create_time,first_name,last_name,role_id,email,birth_date from user WHERE id=?";
    
	public UserDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * Get the connection to the database.
	 * @return DataSource instance
	 * @throws SQLException
	 */
	Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
	
	@Override
	public synchronized Optional<User> registrate(User user) throws DbException {
		LOG.debug("UserDaoImpl.registrate user: " + user);
		Optional<User> optUser = Optional.empty();
		if (user == null) {
			return optUser;
		}
		if (userExists(user)) {
			throw new DbException("User with the email " + user.getEmail() + " already exists!");	
		}
		
		try(Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement(INSERT_NEW_USER, Statement.RETURN_GENERATED_KEYS);) {		
			stmt.setString(1, Util.generateHash(user.getPassword()));
			stmt.setString(2, user.getFirstName());
			stmt.setString(3, user.getLastName());
			stmt.setInt(4, user.getRoleId());
			stmt.setString(5, user.getEmail());
			stmt.setString(6, user.getBirthDate().toString());
			boolean res = stmt.execute();
			int id = -1;
			ResultSet genKeys = stmt.getGeneratedKeys();
			if ((genKeys != null) && (genKeys.next())) {
			    id = genKeys.getInt(1);
			}
			if (id < 1) {
				throw new SQLException("User registration failed!");
			}
			optUser = getUserById(id);
			LOG.debug("User has registrated successfully with id: " + id);
		} catch (SQLException e) {
			LOG.error("UserDaoImpl.registrate user ERROR: " + e.getMessage());
			throw new DbException(e.getMessage());
		}
		
		return optUser;
	}
	
	@Override
	public Optional<User> login(String uid, String password) throws DbException {
		LOG.debug("UserDaoImpl.login uid: " + uid);
		Optional<User> optUser = Optional.empty();
		if ((uid == null) || (uid.isBlank()) || 
				(password == null) || (password.isBlank())) {
			return optUser;
		}
		
		try(Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement(INSERT_NEW_USER, Statement.RETURN_GENERATED_KEYS);) {		
			Optional<User> user = getUserByEmail(uid);
			String errMsg = "Incorrect email or password!";
			if (user.isEmpty()) {
				LOG.debug("There is no user with uid: " + uid);
				throw new SQLException(errMsg);
			}
			User u = user.get();
			LOG.debug("Found user: " + user);
			String hash = Util.generateHash(password);
			if (!(hash.equals(u.getPassword()))) {
				LOG.debug("Incorrect password for the uid: " + uid + 
						". Entered pwd=" + hash + ". DB pwd=" + u.getPassword());
				throw new SQLException(errMsg);
			}
			optUser = user;
			LOG.debug("User has logged successfully: " + u);
		} catch (SQLException e) {
			LOG.error("UserDaoImpl.login ERROR: " + e.getMessage());
			throw new DbException(e.getMessage());
		}
		
		return optUser;
	}
	
	/**
	 * Check if a user exists. Email must be unique, so checking by email.
	 * @param user
	 * @return boolean
	 */
	private boolean userExists(User user) throws DbException {
		if (user == null) {
			return false;
		}
		return getUserByEmail(user.getEmail()).isPresent();
	}

	/**
	 * Get user by email.
	 * @param email user's email.
	 * @return Optional<User>
	 * @throws DbException
	 */
	public Optional<User> getUserByEmail(String email) throws DbException {
		Optional<User> user = Optional.empty();
		if ((email == null) || (email.isEmpty())) {
			return user;
		}
		
		try(Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement(SELECT_USER_BY_EMAIL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);) {
			stmt.setString(1, email);
			LOG.debug("getUserByEmail SQL: " + SELECT_USER_BY_EMAIL);
			ResultSet rs = stmt.executeQuery();
			rs.last();
			LOG.debug("getUserByEmail resultset size: " + rs.getRow());
			if (rs.getRow() > 1) {
				throw new SQLException("There are more than one user registered with Email " + email);
			}
			//rs.beforeFirst();
			//rs.next();
			if (rs.getRow() == 1) {	
				User u = new User(rs.getInt("id"),						rs.getString("email"),
						rs.getString("password"),
						Timestamp.valueOf(rs.getString("birth_date")),
						rs.getString("first_name"),
						rs.getString("last_name"),
						Timestamp.valueOf(rs.getString("create_time")),
						rs.getInt("role_id")
						);
				user = Optional.of(u);
				LOG.debug("getUserByEmail user: " + user);
			}
		} catch (SQLException e) {
			LOG.error("UserDaoImpl.getUserByEmail ERROR: " + e.getMessage());
			throw new DbException(e.getMessage());
		}
		return user;
	}
	
	/**
	 * Get user by id.
	 * @param id is a primary key value of the user table.
	 * @return Optional<User>
	 * @throws DbException
	 */
	public Optional<User> getUserById(int id) throws DbException {
		Optional<User> user = Optional.empty();
		if (id < 1) {
			return user;
		}
		
		try(Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement(SELECT_USER_BY_ID);) {
			stmt.setInt(1, id);
			LOG.debug("getUserById SQL: " + SELECT_USER_BY_ID);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {	
				User u = new User(rs.getInt("id"),
						rs.getString("email"),
						rs.getString("password"),
						Timestamp.valueOf(rs.getString("birth_date")),
						rs.getString("first_name"),
						rs.getString("last_name"),
						Timestamp.valueOf(rs.getString("create_time")),
						rs.getInt("role_id")
						);
				user = Optional.of(u);
				LOG.debug("getUserById user: " + user);
			}
		} catch (SQLException e) {
			LOG.error("UserDaoImpl.getUserByEmail ERROR: " + e.getMessage());
			throw new DbException(e.getMessage());
		}
		return user;
	}
}
