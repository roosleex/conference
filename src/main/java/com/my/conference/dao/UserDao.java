package com.my.conference.dao;

import java.util.Optional;

import com.my.conference.entity.User;

/**
 * User dao.
 * @author Ruslan
 */
public interface UserDao {
	/**
	 * Registrate a new user.
	 * @param user
     * @return Optional<User> of new user.
	 * @throws DbException
	 */
	Optional<User> registrate(User user) throws DbException;
	
	/**
	 * User's login action.
	 * @param uid user's identifier (email).
	 * @param password user's password.
     * @return Optional<User> of found user.
	 * @throws DbException
	 */
	Optional<User> login(String uid, String password) throws DbException;
}
